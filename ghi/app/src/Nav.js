import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/">Automobile Inventory</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/automobiles/create">Add a new Automobile</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models">Models</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/models/create">Add a new Vehicle Model</NavLink>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="/#" id="navbarDropdownSales" role="button" data-bs-toggle="dropdown" aria-expanded="false">Technicians</a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdownSales">
                <li>
                  <NavLink className="dropdown-item" to="/technicians">Technicians</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/technicians/create">Add a Technician</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="/#" id="navbarDropdownSales" role="button" data-bs-toggle="dropdown" aria-expanded="false">Service Appointment</a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdownSales">
                <li>
                  <NavLink className="dropdown-item" to="/appointments">Service Appointments</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/appointments/create">Create a Service Appointment</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/appointments/history">Service History</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="/#" id="navbarDropdownSales" role="button" data-bs-toggle="dropdown" aria-expanded="false">Manufacturers</a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdownSales">
                <li>
                  <NavLink className="dropdown-item" to="/manufacturers">Manufacturers</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/manufacturers/create">Create a manufacturer</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="/#" id="navbarDropdownSales" role="button" data-bs-toggle="dropdown" aria-expanded="false">Salespeople</a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdownSales">
                <li>
                  <NavLink className="dropdown-item" to="/salespeople/">Salespeople</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/salespeople/history/">Salesperson History</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/salespeople/create/">Add a new Salesperson</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="/#" id="navbarDropdownSales" role="button" data-bs-toggle="dropdown" aria-expanded="false">Customers</a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdownSales">
                <li>
                  <NavLink className="dropdown-item" to="/customers/">Customers</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/customers/create/">Add a new Customer</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="/#" id="navbarDropdownSales" role="button" data-bs-toggle="dropdown" aria-expanded="false">Automobile Sales</a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdownSales">
                <li>
                  <NavLink className="dropdown-item" to="/sales/">Available Sales</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/sales/create/">Add a new Sale</NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
