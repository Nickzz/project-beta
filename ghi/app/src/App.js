import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ListSalespeople from './SalesComponents/ListSalespeople';
import SalespersonHistory from './SalesComponents/SalespersonHistory';
import SalespersonForm from './SalesComponents/SalespersonForm';
import TechList from './ServiceComponents/TechList';
import TechForm from './ServiceComponents/TechForm';
import AppForm from './ServiceComponents/AppForm';
import AppList from './ServiceComponents/AppList';
import ListCustomers from './SalesComponents/ListCustomers';
import CustomerForm from './SalesComponents/CustomerForm';
import ListSales from './SalesComponents/ListSales';
import SaleForm from './SalesComponents/SaleForm';
import AppHistory from './ServiceComponents/AppHistory';
import ListAutomobiles from './InventoryComponents/ListAutomobiles';
import AutomobileForm from './InventoryComponents/AutomobileForm';
import VehicleModelForm from './InventoryComponents/VehicleModelForm';
import ListManufactures from './InventoryComponents/ListManufactures';
import ManufacturerForm from './InventoryComponents/ManufacturesForm';
import ListVehicles from './InventoryComponents/ListVehicles';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/automobiles" element={<ListAutomobiles />}></Route>
          <Route path="/automobiles/create/" element={<AutomobileForm />}></Route>
          <Route path="/models/" element={<ListVehicles />}></Route>
          <Route path="/models/create/" element={<VehicleModelForm />}></Route>
          <Route path="/technicians">
            <Route index element={<TechList />} />
            <Route path='create' element={<TechForm />} />
          </Route>
          <Route path="/appointments">
            <Route index element={<AppList />} />
            <Route path='create' element={<AppForm />} />
            <Route path='history' element={<AppHistory />} />
          </Route>
          <Route path="salespeople">
            <Route index element={<ListSalespeople />} />
            <Route path="history" element={<SalespersonHistory />} />
            <Route path="create" element={<SalespersonForm />} />
          </Route>
          <Route path="customers">
            <Route index element={<ListCustomers />} />
            <Route path="create" element={<CustomerForm />} />
          </Route>
          <Route path="sales">
            <Route index element={<ListSales />} />
            <Route path="create" element={<SaleForm />} />
          </Route>
          <Route path="/manufacturers">
            <Route index element={<ListManufactures />} />
            <Route path="create" element={<ManufacturerForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
