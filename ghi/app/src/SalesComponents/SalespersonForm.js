import React, { useEffect, useState } from 'react';

function SalespersonForm() {

    // first name handler
    const [firstName, setFirstName] = useState('');
    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    };

    // last name handler
    const [lastName, setLastName] = useState('');
    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    };

    // employeeID handler
    const [employeeID, setEmployeeID] = useState('');
    const handleEmployeeIDChange = (event) => {
        const value = event.target.value;
        setEmployeeID(value);
    };

    // submit handler
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeID;

        const salespersonUrl = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(salespersonUrl, fetchConfig);
        if (response.ok) {
            setFirstName('');
            setLastName('');
            setEmployeeID('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        await fetch(url);
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a New Salesperson</h1>
            <form onSubmit={handleSubmit} id="create-salesperson-form">
              <div className="form-floating mb-3">
                <input onChange={handleFirstNameChange} placeholder="first Name" required type="text" name="first name" id="first name" className="form-control" value={firstName}/>
                <label htmlFor="first name">Enter Representative's First Name...</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleLastNameChange} placeholder="last name" required type="text" name="last name" id="last name" className="form-control" value={lastName}/>
                <label htmlFor="last name">Enter Representative's Last Name...</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEmployeeIDChange} placeholder="employeeID" required type="text" name="employeeID" id="employeeID" className="form-control" value={employeeID}/>
                <label htmlFor="employeeID">Enter Representative's Employee ID...</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
        </div>
);}


export default SalespersonForm;
