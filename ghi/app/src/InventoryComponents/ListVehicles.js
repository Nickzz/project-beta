import { useEffect, useState } from 'react';

function ListVehicles() {
    const [vechLists, setVechs] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/models/');

        if (response.ok) {
            const data = await response.json();
            setVechs(data.models);
        }
    }

    useEffect(() => {
        getData()
    }, []);

    return (
        <div>
            <h1>Models</h1>
            <table className="table table-striped table align-middle" style={{ width: '100%', tableLayout: 'fixed' }}>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody >
                    {vechLists.map(vech => {
                        return (
                            <tr style={{ height: '60px' }} key={vech.href}>
                                <td>{vech.name}</td>
                                <td>{vech.manufacturer.name}</td>
                                <td>
                                    <img src={vech.picture_url} alt="car" />
                                </td>
                            </tr>
                        );
                    })}

                </tbody>
            </table>
        </div>
    )
}
export default ListVehicles;
