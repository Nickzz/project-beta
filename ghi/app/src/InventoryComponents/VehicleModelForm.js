import React, { useEffect, useState } from 'react';


function VehicleModelForm() {

    const [manufacturers, setManufacturers] = useState([])
    const [manufacturer, setManufacturer] = useState('');
    const [name, setName] = useState('');
    const [pictureURL, setPictureURL] = useState('');

    // manufacturer handler
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    };

    // name handler
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    };

    // picture url handler
    const handlePictureURLChange = (event) => {
        const value = event.target.value;
        setPictureURL(value);
    };

    // submit handler
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.manufacturer_id = manufacturer;
        data.name = name;
        data.picture_url = pictureURL;

        const vehicleUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(vehicleUrl, fetchConfig)

        if (response.ok) {
            setManufacturer('');
            setName('');
            setPictureURL('');
        }
    }

    const fetchData = async () => {

        const ManufacturerUrl = 'http://localhost:8100/api/manufacturers/';

        const ManufacturerResponse = await fetch(ManufacturerUrl);

        if (ManufacturerResponse.ok) {
            const ManufacturerData = await ManufacturerResponse.json();
            setManufacturers(ManufacturerData.manufacturers)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-3">
                    <h1>Add Vehicle Model</h1>
                    <form onSubmit={handleSubmit} id="create-sales-form">
                        <div className="mb-3">
                            <select onChange={handleManufacturerChange} required name="automobile" id="automobile" className="form-select" value={manufacturer}>
                            <option>Choose a Manufacturer...</option>
                            {manufacturers.map(manufacturer => {
                                return (
                                    <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                )
                            })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="name" required type="text" name="name" id="name" className="form-control" autoComplete="false" value={name} />
                            <label htmlFor="name">Enter the Model Name...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureURLChange} placeholder="pictureURL" required type="url" name="pictureURL" id="pictureURL" className="form-control" value={pictureURL} />
                            <label htmlFor="pictureURL">Enter the Model Picture's URL...</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}


export default VehicleModelForm;
