import React, { useEffect, useState } from "react";



const ListAutomobiles = () => {

    // automobiles state
    const [automobiles, setAutomobiles] = useState([]);

    const fetchAutomobiles = async () => {

        try {

            const response = await fetch("http://localhost:8100/api/automobiles/");

            if (response.ok) {
                const data = await response.json();
                setAutomobiles(data.autos);
            } else {
                console.error('Failed to fetch Automobiles')
            }

        } catch (error) {

          console.error(error.message);

        }
    };

    useEffect(() => {
      fetchAutomobiles();
    }, []);

    return (
        <div className="container mt-4">
            <h2>Automobiles</h2>
            <table className="table table-striped table-dark text-center">
                <thead>
                    <tr>
                        <th scope="col">VIN</th>
                        <th scope="col">Color</th>
                        <th scope="col">Year</th>
                        <th scope="col">Model</th>
                        <th scope="col">Manufacturer</th>
                        <th scope="col">Thumbnail</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles.map((automobile) =>
                        <tr key={automobile.id}>
                            <td>{automobile.vin}</td>
                            <td>{automobile.color}</td>
                            <td>{automobile.year}</td>
                            <td>{automobile.model.name}</td>
                            <td>{automobile.model.manufacturer.name}</td>
                            <td>
                                <img src={automobile.model.picture_url} alt={automobile.model.name} className="img-thumbnail" style={{width: "200px", height: "150px"}} />
                            </td>

                            <td>{automobile.sold ? "SOLD" : "AVAILABLE"}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    );
};


export default ListAutomobiles;
