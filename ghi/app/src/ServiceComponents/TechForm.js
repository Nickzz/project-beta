import React, { useState, useEffect } from 'react';

function TechForm() {
    // showSuccess is a state used to show if form is submitted succesfully
    const [showSuccess, setShowSuccess] = useState(false);
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: ''
    });

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = "http://localhost:8080/api/technicians/";

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        try {
            const response = await fetch(url, fetchConfig);
            if (response.ok) {
                setFormData(
                    {
                        first_name: '',
                        last_name: '',
                        employee_id: ''
                    }
                );
                setShowSuccess(true);
            }
        } catch (error) {
            console.error('Failed to submit:', error);
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a technician</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.first_name} placeholder="Name" required type="text" name="first_name" id="first_name" className="form-control" autoComplete="off" />
                            <label htmlFor="first_name">First name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.last_name} placeholder="style" required type="text" name="last_name" id="last_name" className="form-control" />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.employee_id} placeholder="color" required type="number" name="employee_id" id="employee_id" className="form-control" />
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                    {showSuccess && (
                        <div className="alert alert-success" role="alert">
                            Form submitted successfully!
                        </div>
                    )}
                </div>
            </div>
        </div>
    )
}
export default TechForm;
