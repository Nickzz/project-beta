import { useEffect, useState } from 'react';

function AppList() {
    const [showCancelSuccess, setCancelSuccess] = useState(false);
    const [showFinishSuccess, setFinishSuccess] = useState(false);
    const [appslists, setApps] = useState([]);

    const getData = async () => {
        //get data of appointment lists
        const response = await fetch('http://localhost:8080/api/appointments/');

        if (response.ok) {
            const data = await response.json();
            let appList = [];
            for (let app of data.appointments) {
                if (app.status != "canceled" && app.status != "finished") {
                    appList.push(app);
                }
            }
            setApps(appList);
        }
    }

    useEffect(() => {
        getData()
    }, []);

    const handleCancelClick = async (app) => {
        const parts = app.href.split('/');
        const number = parts[3];
        const url = `http://localhost:8080/api/appointments/${number}/cancel/`;

        const fetchConfig = {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
            }
        };

        try {
            const response = await fetch(url, fetchConfig);
            if (response.ok) {
                const newAppList = appslists.filter((a) => a.href != app.href);
                setApps(newAppList);
                setCancelSuccess(true);
                setFinishSuccess(false);
            }
        } catch (error) {
            console.error('Failed to submit:', error);
        }
    }

    const handleFinishedClick = async (app) => {
        const parts = app.href.split('/');
        const number = parts[3];
        const url = `http://localhost:8080/api/appointments/${number}/finish/`;

        const fetchConfig = {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
            }
        };

        try {
            const response = await fetch(url, fetchConfig);
            if (response.ok) {
                const newAppList = appslists.filter((a) => a.href != app.href);
                setApps(newAppList);
                setFinishSuccess(true);
                setCancelSuccess(false);
            }
        } catch (error) {
            console.error('Failed to submit:', error);
        }
    }


    return (
        <div>
            <h1 className="mt-5">Appointments
                {showCancelSuccess && (
                    <div className="alert alert-success" role="alert">
                        cancelled successfully!
                    </div>
                )}
                {showFinishSuccess && (
                    <div className="alert alert-success" role="alert">
                        finish successfully!
                    </div>
                )}
            </h1>
            <table className="table table-striped table  mt-3" style={{ width: '100%', tableLayout: 'fixed' }}>
                <colgroup>
                    <col style={{ width: '20%' }} />
                    <col style={{ width: '10%' }} />
                    <col style={{ width: '10%' }} />
                    <col style={{ width: '15%' }} />
                    <col style={{ width: '10%' }} />
                    <col style={{ width: '10%' }} />
                    <col style={{ width: '10%' }} />
                </colgroup>
                <thead>
                    <tr>
                        <th style={{ padding: '8px', textAlign: 'left' }}>VIN</th>
                        <th>Customer Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician Name</th>
                        <th>Reason</th>
                        <th>Is VIP?</th>
                    </tr>
                </thead>
                <tbody >
                    {appslists.map(app => {
                        return (
                            <tr style={{ height: '60px' }} key={app.href}>
                                <td>{app.vin}</td>
                                <td>{app.customer}</td>
                                <td>{app.date_time.split('T')[0]}</td>
                                <td>{app.date_time.split('T')[1]}</td>
                                <td>{app.technician.first_name} {app.technician.last_name}</td>
                                <td>{app.reason}</td>
                                <td>{app.vip}</td>
                                <td><button onClick={() => handleCancelClick(app)} type="button" className="btn btn-danger">Cancel</button></td>
                                <td><button onClick={() => handleFinishedClick(app)} type="button" className="btn btn-success">Finished</button></td>
                            </tr>
                        );
                    })}

                </tbody>
            </table>
        </div>
    )
}
export default AppList;
