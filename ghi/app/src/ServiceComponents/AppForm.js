import React, { useState, useEffect } from 'react';

function AppForm() {
    const [showSuccess, setShowSuccess] = useState(false);
    const [technicians, setTech] = useState([]);
    const [formData, setFormData] = useState({
        vin: '',
        customer: '',
        date: '',
        time: '',
        technician: '',
        reason: ''
    });

    const getData = async () => {
        const url = "http://localhost:8080/api/technicians/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTech(data.technicians);
        }
    }

    useEffect(() => {
        getData();
    }, []
    );


    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = "http://localhost:8080/api/appointments/";

        const submitData = {
            vin: formData.vin,
            customer: formData.customer,
            date_time: `${formData.date} ${formData.time}`,
            technician: formData.technician,
            reason: formData.reason
        };

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(submitData),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        try {
            const response = await fetch(url, fetchConfig);
            if (response.ok) {
                setFormData(
                    {
                        vin: '',
                        customer: '',
                        date: '',
                        time: '',
                        technician: '',
                        reason: ''
                    }
                );
                setShowSuccess(true);
            }
        } catch (error) {
            console.error('Failed to submit:', error);
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a service appointment</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin">Automobile VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.customer} placeholder="customer" required type="text" name="customer" id="customer" className="form-control" />
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.date} placeholder="date" required type="date" name="date" id="date" className="form-control" />
                            <label htmlFor="date">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.time} placeholder="time" required type="time" name="time" id="time" className="form-control" />
                            <label htmlFor="time">Time</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} value={formData.technician} required name="technician" id="technician" className="form-select">
                                <option value="">Choose a technician</option>
                                {technicians.map(technician => {
                                    return (
                                        <option key={technician.employee_id} value={technician.employee_id}>{technician.first_name} {technician.last_name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.reason} placeholder="reason" required type="text" name="reason" id="reason" className="form-control" />
                            <label htmlFor="reason">Reason</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                    {showSuccess && (
                        <div className="alert alert-success" role="alert">
                            Form submitted successfully!
                        </div>
                    )}
                </div>
            </div>
        </div>
    )
}
export default AppForm;
