from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from django.shortcuts import get_object_or_404
from .models import Technician, AutomobileVO, Appointment


class TechListEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "employee_id"]


class AppListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        "vip",
    ]
    encoders = {
        "technician": TechListEncoder(),
    }


# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        techs = Technician.objects.all()
        return JsonResponse({"technicians": techs}, encoder=TechListEncoder)
    else:
        content = json.loads(request.body)

        technician = Technician.objects.create(**content)
        return JsonResponse(technician, encoder=TechListEncoder, safe=False)


@require_http_methods(["DELETE"])
def api_show_technicians(request, pk):
    if request.method == "DELETE":
        tech = Technician.objects.get(id=pk)
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        apps = Appointment.objects.all()
        return JsonResponse({"appointments": apps}, encoder=AppListEncoder)
    else:
        content = json.loads(request.body)

        try:
            tech_id = content["technician"]
            technician = get_object_or_404(
                Technician,
                employee_id=tech_id,
            )
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=400,
            )

        app = Appointment.objects.create(**content)
        return JsonResponse(app, encoder=AppListEncoder, safe=False)


@require_http_methods(["DELETE"])
def api_show_appointments(request, pk):
    if request.method == "DELETE":
        app = get_object_or_404(Appointment, id=pk)
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    try:
        app = get_object_or_404(Appointment, id=pk)
        app.status = "canceled"
        app.save()
        return JsonResponse(
            {"message": "Appointment cancelled successfully"}, status=200
        )
    except Appointment.DoesNotExist:
        return JsonResponse({"error": "Appointment not found"}, status=404)


@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    try:
        app = get_object_or_404(Appointment, id=pk)
        app.status = "finished"
        app.save()
        return JsonResponse(
            {"message": "Appointment finished successfully"}, status=200
        )
    except Appointment.DoesNotExist:
        return JsonResponse({"error": "Appointment not found"}, status=404)
