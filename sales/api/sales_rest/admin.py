from django.contrib import admin
from django.db import models
from .models import AutomobileVO, Salesperson, Customer, Sale


# Admin Models:
# AutomobileVo Admin
@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    import_href = models.TextField(max_length=200, unique=True, default=False)  # type: ignore
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


# Salesperson Admin
@admin.register(Salesperson)
class SalespersonAdmin(admin.ModelAdmin):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=100)


# Customer Admin
@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200)


# Sales Admin
@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.CASCADE,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="salesperson",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="customer",
        on_delete=models.CASCADE,
    )
    price = models.CharField(max_length=200)
