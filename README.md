# CarCar:


## TOC

- [CarCar](#carcar)
  - [Team](#team)
  - [Setup Instructions](#how-to-run-this-app)
  - [Design](#diagram)
	- [API Documentation](#api-documentation)
		- [Project Beta URLs](#urls-and-ports)
		- [Inventory API](#inventory-microservice-and-api)
			- [Manufacturer Model](#manufacturer---crud-documentation)
			- [VehicleModel Model](#vehiclemodel---crud-documentation)
			- [Automobile Model](#automobile---crud-documentation)
		- [Service API](#service-api)
			- [Technician Model](#technician---crud-documentation)
			- [Appointment Model](#appointment---crud-documentation)
		- [Sales API](#auto-sales-microservice-and-api)
			- [Salesperson Model](#salesperson---crud-documentation)
			- [Customer Model](#customers---crud-documentation)
			- [Sale Model](#sales---crud-documentation)
	- [Value Objects](#value-objects)

## Team:

* **Nick Zheng** - Automobile Service
* **Benjamin Stults** - Auto Sales Microservice

## How to Run this App
 - Fork the repository
    - Be sure to copy the applicable "Clone with HTTPS" or "Clone with SSH"

 - Navigate to directory in your terminal that you want to clone the repository to.
    - Clone the repository: git clone <<repository.clone.url.here>>
    - Change directory to the cloned repository: cd project-beta
 	- Build and run the project using Docker with the following commands:
    	- Replace docker-compose up with docker-compose up -d if you want to run in detached mode
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- Ensure all your containers are running via Docker Desktop
- View the project at the given urls:
- CarCar Main Page: http://localhost:3000/

## Diagram
![Img](Project%20Beta%20DDD.png)

## API Documentation

### URLs and Ports
| API | PORT | URL
| ----------- | ----------- | ----------- |
| Inventory API | 8100 | http://localhost:8100/api/
| SERVICES API | 8080 | http://localhost:8100/api/
| SALES API | 8090 | http://localhost:8090/api/
| React API | 3000 | http://localhost:3000/

## Inventory MicroService and API

 - The Inventory Microservice backend is comprised of 3 models:
	- Manufacturer:
		- name = models.CharField(max_length=100, unique=True)
	- VehicleModel:
		- name = models.CharField(max_length=100)
    	- picture_url = models.URLField()
     	- manufacturer = models.ForeignKey(
        	Manufacturer,
        	related_name="models",
        	on_delete=models.CASCADE,
    	)
	- Automobile:
		- color = models.CharField(max_length=50)
    	- year = models.PositiveSmallIntegerField()
    	- vin = models.CharField(max_length=17, unique=True)
    	- sold = models.BooleanField(default=False)
    	- model = models.ForeignKey(
        	VehicleModel,
        	related_name="automobiles",
        	on_delete=models.CASCADE,
    	)

### Manufacturer - CRUD Documentation:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/int:id/
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/int:id/
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/int:id/

JSON Body to send data:

- Create or update a manufacturer: (NOTE: only need name for update)

```
{
	"name": "Some Car Brand Here"
}
```

JSON Body returned data:

- List manufacturers:
```
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Some Car Brand Here"
		},
	]
}
```

- GET a manufacturer by <<int:id>>:
```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Some Car Brand Here"
}
```

- Update a manufacturer by <<int:id>>:
```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Updated Car Brand Here"
}
```

- Delete a manufacturer by <<int:id>>:
```
{
	"id": null,
	"name": "Bugatti"
}
```

### VehicleModel - CRUD Documentation:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List vehiclemodels | GET | http://localhost:8100/api/models/
| Create a vehiclemodel | POST | http://localhost:8100/api/models/
| Get a specific vehiclemodel | GET | http://localhost:8100/api/models/int:id/
| Update a specific vehiclemodel | PUT | http://localhost:8100/api/models/int:id/
| Delete a specific vehiclemodel | DELETE | http://localhost:8100/api/models/int:id/

JSON Body to send data:

- Create a vehiclemodel:
```
{
  "name": "VehicleModel Name Here",
  "picture_url": "some.url.here",
  "manufacturer_id": 1
}
```

- Update a vehiclemodel:
```
{
  "name": "New VehicleModel Name Here",
  "picture_url": "some.url.here"
}
```

JSON Body returned data:

- List vehiclemodels:
```
{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Some VehicleModel Name",
			"picture_url": "some.url.here",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Some Car Brand"
			}
		},
	]
}
```

- GET a vehiclemodel by <<int:id>>:
```
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Some VehicleModel Name",
	"picture_url": "some.url.here",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Some Car Brand"
	}
}
```

- Update a vehiclemodel by <<int:id>>:
```
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "New VehicleModel Name",
	"picture_url": "some.url.here",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Some Car Brand"
	}
}
```

- Delete a vehiclemodel by <<int:id>>: (NOTE: "id" will be null on success)
```
{
	"id": null,
	"name": "Some VehicleModel Name",
	"picture_url": "some.url.here",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Some Car Brand"
	}
}
```

### Automobile - CRUD Documentation:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/vin/
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/vin/
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/vin/

JSON Body to send data:

- Create an automobile:
```
{
  "color": "Color",
  "year": 2024,
  "vin": "some.valid.vin.here",
  "model_id": 1
}
```

- Update an automobile:
```
{
  "color": "red",
  "year": 2024,
  "sold": true
}
```

JSON Body returned data:

- List automobiles:
```
{
	"autos": [
		{
			"href": "/api/automobiles/<<vin>>/",
			"id": 1,
			"color": "Color",
			"year": 2024,
			"vin": "some.vin.here",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Some VehicleModel Name",
				"picture_url": "some.url",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Some Car Brand"
				}
			},
			"sold": false
		},
	]
}
```

- GET an automobile by <<vin>>:
```
{
	"autos": [
		{
			"href": "/api/automobiles/<<vin>>/",
			"id": 1,
			"color": "Color",
			"year": 2024,
			"vin": "some.vin.here",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Some VehicleModel Name",
				"picture_url": "some.url",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Some Car Brand"
				}
			},
			"sold": false
		},
	]
}
```

- Update an automobile by <<vin>>:
```
{
	"autos": [
		{
			"href": "/api/automobiles/<<vin>>/",
			"id": 1,
			"color": "New Color",
			"year": 2024, (new year if changed)
			"vin": "some.vin.here",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Some VehicleModel Name",
				"picture_url": "some.url",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Some Car Brand"
				}
			},
			"sold": false (boolean change if changed)
		},
	]
}
```

- Delete an automobile by <<vin>>: (NOTE: "id" will be null on success)
```
{
	"autos": [
		{
			"href": "/api/automobiles/<<vin>>/",
			"id": 1,
			"color": "Color",
			"year": 2024,
			"vin": "some.vin.here",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Some VehicleModel Name",
				"picture_url": "some.url",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Some Car Brand"
				}
			},
			"sold": false
		},
	]
}
```

## Service API
 - Put Service API documentation here

### Service API
 - The Service MicroService backend is comprised of 3 models:
	- Technician:
		- Fields:
			- first_name = models.CharField(max_length=100)
    		- last_name = models.CharField(max_length=100)
    		- employee_id = models.PositiveSmallIntegerField()
	- AutomobileVO:
		- Fields:
			- vin = models.CharField(max_length=17)
    		- sold = models.CharField(max_length=100)
	- Appointment:
		- Fields:
			- date_time = models.DateTimeField()
    		- reason = models.CharField(max_length=100)
    		- status = models.CharField(max_length=100, default="current")
    		- vin = models.CharField(max_length=17)
    		- customer = models.CharField(max_length=100)
    		- technician = models.ForeignKey(Technician, related_name="appointments", on_delete=models.CASCADE, null=True)
    		- vip = models.CharField(max_length=3, default="No")

### Technician - CRUD Documentation:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technician | GET | http://localhost:8080/api/technicians/
| Create a technician | POST | http://localhost:8080/api/technicians/
| Delete a specific technician | DELETE | http://localhost:8080/api/technicians/:id/

JSON Body to send data:

- Create a technician:
```
{
  "first_name": "tina",
  "last_name": "zz",
  "employee_id": 6
}

```

JSON Body returned data:
- List Technicians:
```
{
    "technicians": [
        {
            "href": "/api/technicians/3/",
            "first_name": "jay",
            "last_name": "chou",
            "employee_id": 3
        }
    ]
}
```

- Delete a technician by <<int:id>>:
```
{
    "deleted": true
}
```

### Appointment - CRUD Documentation:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List appointments | GET | http://localhost:8080/api/appointments/
| Create an appointment | POST | http://localhost:8080/api/appointments/
| Delete an appointment | DELETE | http://localhost:8080/api/appointments/:id/
| Cancel an appointment | PUT | http://localhost:8080/api/appointments/:id/cancel/
| Finish an appointment | PUT | http://localhost:8080/api/appointments/:id/finish/

JSON Body to send data:

- Create an appointment:
```
{
    "date_time": "2021-09-15 13:45",
    "reason": "tire explode",
    "status": "waited",
    "vin": 666,
    "customer": "john",
    "technician": 3
}
```

JSON Body returned data:

- List appointments:
```
{
    "appointments": [
        {
            "href": "/api/appointments/9/",
            "date_time": "2024-05-13T06:43:00+00:00",
            "reason": "gas change ",
            "status": "current",
            "vin": "999999999",
            "customer": "ffff",
            "technician": {
                "href": "/api/technicians/7/",
                "first_name": "aaaa",
                "last_name": "bbbb",
                "employee_id": 9
            },
            "vip": "No"
        }
    ]
}
```

- Delete an appointment by <<int:id>>:
```
{
    "deleted": true
}
```

## Auto Sales MicroService and API

- The Sales MicroService backend is comprised of 4 models:
	- Salesperson:
		- Fields:
			- first_name = models.CharField(max_length=50)
    		- last_name = models.CharField(max_length=50)
    		- employee_id = models.CharField(max_length=25)
	- Customer:
		- Fields:
			- first_name = models.CharField(max_length=50)
    		- last_name = models.CharField(max_length=50)
    		- address = models.TextField()
    		- phone_number = models.CharField(max_length=12)
	- Sale:
		- Fields:
			- price = models.CharField(max_length=25)
    		- automobile = models.ForeignKey(
        		AutomobileVO,
        		related_name="automobile",
        		on_delete=models.CASCADE,
    		)
    		- salesperson = models.ForeignKey(
        		Salesperson,
        		related_name="salesperson",
        		on_delete=models.CASCADE,
    		)
    		- customer = models.ForeignKey(
        		Customer,
        		related_name="customer",
        		on_delete=models.CASCADE,
    		)
	- AutomobileVO:
		- Fields:
			- import_href = models.TextField(max_length=100, unique=True, 		default=False)
    		- vin = models.CharField(max_length=17, unique=True)
    		- sold = models.BooleanField(default=False)

### Salesperson - CRUD Documentation:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople | GET | http://localhost:8090/api/salespeople/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| Get a specific salesperson | GET | http://localhost:8090/api/salespeople/int:id/
| Delete a specific salesperson | DELETE | http://localhost:8090/api/salespeople/int:id/

JSON Body to send data:

- Create a salesperson:
```
{
	"first_name": "John",
	"last_name": "Doe",
	"employee_id": "1"
}
```

JSON Body returned data:

- List Salespeople:
```
{
	"salespeople": [
		{
			"first_name": "John",
			"last_name": "Doe",
			"employee_id": "1",
			"id": 1
		},
    ]
}
```

- Salesperson by <int:id>:
```
{
	"first_name": "John",
	"last_name": "Doe",
	"employee_id": "1",
	"id": 1
}
```

- Delete a Salesperson by <<int:id>>:
```
{
	"deleted": true
}
```

### Customer - CRUD Documentation:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List customers | GET | http://localhost:8090/api/customers/
| Create a customer | POST | http://localhost:8090/api/customers/
| Get a specific customer | GET | http://localhost:8090/api/customers/int:id/
| Delete a specific customer | DELETE | http://localhost:8090/api/customers/int:id/


JSON Body to send data:

- Create a customer:
```
{
	"first_name": "Jane",
	"last_name": "Doe",
	"address": "Some Address",
	"phone_number": "169726374628"
}
```

JSON Body returned data:

- List Customers:
```
{
	"customers": [
		{
			"first_name": "Jane",
			"last_name": "Doe",
			"address": "Some Address",
			"phone_number": "166296519171",
			"id": 1
		},
    ]
}
```

- Customer by <<int:id>>:
```
{
	"first_name": "Jane",
	"last_name": "Doe",
	"address": "Some Adress",
	"phone_number": "166296519171",
	"id": 1
}
```

- Delete a Customer by <int:id>:
```
{
	"deleted": true
}
```

### Sale - CRUD Documentation:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List sales | GET | http://localhost:8090/api/sales/
| Create a sale | POST | http://localhost:8090/api/sales/
| Delete a specific sale | DELETE | http://localhost:8090/api/sales/int:id/


JSON Body to send data:

- Create a sale:
```
{
	"price": 123456,
	"automobile": "<<enter.vin.here>>",
	"salesperson": <<int:id.of.salesperson>>,
	"customer": <<int:id.of.customer>>
}

```

JSON Body returned data:

- List Sales:
```
{
	"sales": [
		{
			"salesperson": {
				"first_name": "John",
				"last_name": "Doe",
				"employee_id": "1",
				"id": 1
			},
			"customer": "Jane Doe",
			"automobile": {
				"vin": "<<vin>>",
				"sold": false,
				"id": 1
			},
			"price": "123456",
			"id": 1
		},
    ]
}
```

- Delete a Sale by <int:id>:
```
{
	"deleted": true
}
```

## Value Objects
 - AutomobileVO: This model is necessary within the Sales MicroService in order to create an instance of Sale. AutomobileVO is a value object which gets it's data from referencing the Automobile model that lives within the Inventory API. This is achieved by the poller.py file that polls the Inventory API List Automobiles and updates the data if the AutomobileVO exists, or creates a AutomobileVO if it does not.
- AutomobileVO: The model is in the Service Microservice to get the vin value, which is an attribute in Appointment model. Since Automobile model is in Inventry API which located in another docker, we use poller.py to access that docker. Then we get the data from Automobile model and update or create the data for AutomobileVO.
